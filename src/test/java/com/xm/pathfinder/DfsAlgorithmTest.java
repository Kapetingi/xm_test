package com.xm.pathfinder;

import com.xm.chess.Knight;
import com.xm.chess.Position;
import com.xm.chess.Solution;
import com.xm.dfs.IterativeDeepeningSearch;
import com.xm.dfs.Node;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class DfsAlgorithmTest {
    @Test
    public void testUnreachableState() {
        Position currentPosition = new Position("A1");
        Position goalPosition = new Position("H7");

        Node initNode = new Node(null, new Knight(currentPosition), 0);

        IterativeDeepeningSearch dfs = new IterativeDeepeningSearch();
        Solution solution = dfs.findSolution(initNode, 3, (a) -> a.getFigure().getCurrentPosition().equals(goalPosition));
        assertNull(solution);
    }

    @Test
    public void testReachableStateMoreThan3() {
        Position currentPosition = new Position("A1");
        Position goalPosition = new Position("H7");

        Node initNode = new Node(null, new Knight(currentPosition), 0);

        IterativeDeepeningSearch dfs = new IterativeDeepeningSearch();
        Solution solution = dfs.findSolution(initNode, 7, (a) -> a.getFigure().getCurrentPosition().equals(goalPosition));
        assertNotNull(solution);
        assertEquals("A1->C2->B4->D5->F6->H7", solution.toString());
    }

    @Test
    public void testReachableState() {
        Position currentPosition = new Position("A2");
        Position goalPosition = new Position("B5");

        Node initNode = new Node(null, new Knight(currentPosition), 0);

        IterativeDeepeningSearch dfs = new IterativeDeepeningSearch();
        Solution solution = dfs.findSolution(initNode, 3, (a) -> a.getFigure().getCurrentPosition().equals(goalPosition));
        assertNotNull(solution);
        assertEquals("A2->C3->B5", solution.toString());
    }

    @Test
    public void testReachableStateShort() {
        Position currentPosition = new Position("A1");
        Position goalPosition = new Position("B3");

        Node initNode = new Node(null, new Knight(currentPosition), 0);

        IterativeDeepeningSearch dfs = new IterativeDeepeningSearch();
        Solution solution = dfs.findSolution(initNode, 3, (a) -> a.getFigure().getCurrentPosition().equals(goalPosition));
        assertNotNull(solution);
        assertEquals("A1->B3", solution.toString());
    }
}
