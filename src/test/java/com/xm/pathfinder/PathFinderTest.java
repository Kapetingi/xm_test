package com.xm.pathfinder;

import com.xm.chess.Knight;
import com.xm.chess.Position;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PathFinderTest {

    @Test
    public void testKnightAvailablePosition(){
        Position position = new Position("A1");
        Knight knight = new Knight(position);
        List<Position> availablePositions = knight.getAvailablePositions();
        assertNotNull(availablePositions);
        assertEquals(2,availablePositions.size());
        assertEquals(new Position("B3"), availablePositions.get(0));
        assertEquals(new Position("C2"), availablePositions.get(1));

    }


    @Test
    public void testPositionConvert(){
        Position test = new Position("A2");
        assertEquals(1, test.getX());
        assertEquals(2, test.getY());
    }


}
