package com.xm.chess;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Getter
@Setter
@AllArgsConstructor
public class Position {

    private int x;

    private int y;

    public Position(String x){
        this.x = x.toUpperCase().charAt(0)-64;
        this.y = x.toUpperCase().charAt(1)-48;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;

        if (!(o instanceof Position)) return false;

        Position position = (Position) o;

        return new EqualsBuilder()
                .append(getX(), position.getX())
                .append(getY(), position.getY())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getX())
                .append(getY())
                .toHashCode();
    }

    @Override
    public String toString() {
        char s = (char) (x+64);
        return Character.toString(s)+y;
    }
}
