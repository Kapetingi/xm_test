package com.xm.chess;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class Knight implements Figure {

    private Position currentPosition;

    public List<Position> getAvailablePositions() {
        List<Position> result = new ArrayList<>();
        addIfValid(result,new Position(currentPosition.getX()+1,currentPosition.getY()+2));
        addIfValid(result,new Position(currentPosition.getX()+1,currentPosition.getY()-2));
        addIfValid(result,new Position(currentPosition.getX()+2,currentPosition.getY()+1));
        addIfValid(result,new Position(currentPosition.getX()+2,currentPosition.getY()-1));
        addIfValid(result,new Position(currentPosition.getX()-1,currentPosition.getY()+2));
        addIfValid(result,new Position(currentPosition.getX()-1,currentPosition.getY()-2));
        addIfValid(result,new Position(currentPosition.getX()-2,currentPosition.getY()+1));
        addIfValid(result,new Position(currentPosition.getX()-2,currentPosition.getY()-1));
        return result;
    }

    @Override
    public Position getCurrentPosition() {
        return this.currentPosition;
    }

    private void addIfValid(List<Position> result, Position position) {
        if (position.getX() > 8 || position.getX() < 1 || position.getY() > 8 || position.getY() < 1) {
            return;
        }
        result.add(position);
    }
}
