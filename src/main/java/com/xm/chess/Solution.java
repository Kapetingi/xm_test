package com.xm.chess;

import com.xm.dfs.Node;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Solution {

    private Node solutionNode;

    public Solution(Node solutionNode) {
        this.solutionNode = solutionNode;
    }

    public String toString() {
        Node current = solutionNode;
        StringBuilder result = new StringBuilder(solutionNode.getFigure().getCurrentPosition().toString());
        while ((current = current.getParent()) != null) {
            result.insert(0, current.getFigure().getCurrentPosition() + "->");
        }
        return result.toString();
    }


}
