package com.xm.chess;

import lombok.NonNull;

import java.util.List;

public interface Figure {

    /**
     * Define all available position from current position
     * @return all available position from current position
     */
    @NonNull
    List<Position> getAvailablePositions();

    /**
     * Return current figure position
     * @return current figure position
     */
    @NonNull
    Position getCurrentPosition();


}
