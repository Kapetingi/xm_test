package com.xm.dfs;

import com.xm.chess.Figure;
import com.xm.chess.Knight;
import com.xm.chess.Position;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class Node {

    private Node parent;

    private Figure figure;

    private int depth;

    /**
     * Identifies successors for this node
     * @return successors for this node
     */
    public List<Node> expand() {
        List<Node> result = new ArrayList<>();
        for (Position position : figure.getAvailablePositions()) {
            Node child = new Node(this,  new Knight(position), this.getDepth() + 1);
            result.add(child);
        }
        return result;
    }

}
