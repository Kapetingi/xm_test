package com.xm.dfs;

import com.xm.chess.Solution;
import lombok.NonNull;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.function.Predicate;

public class IterativeDeepeningSearch {

    /**
     * Find solution by depth first search algorithm
     *
     * @param initNode - start searching node
     * @param limit - limit of depth, how much step we allow to find solution
     * @param problem - predicate for check achieving the goal
     * @return solution, path for achieving the goal
     */
    public Solution findSolution(@NonNull Node initNode,
                                 int limit,
                                 @NonNull Predicate<Node> problem){
        Solution shortestPath = null;

        Queue<Node> fringe = new ArrayDeque<>();
        fringe.add(initNode);
        while (!fringe.isEmpty()) {
            List<Node> newGeneration = new ArrayList<>();
            while (!fringe.isEmpty()) {
                Node currentNode = fringe.remove();
                if (problem.test(currentNode)) {
                    shortestPath = new Solution(currentNode);
                } else {
                    if (currentNode.getDepth() < limit) {
                        if(shortestPath!= null && shortestPath.getSolutionNode().getDepth() <= currentNode.getDepth()) {
                            continue;
                        }
                        newGeneration.addAll(currentNode.expand());
                    }
                }
            }
            fringe.addAll(newGeneration);
        }
        return shortestPath;
    }



}
