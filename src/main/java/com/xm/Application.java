package com.xm;

import com.xm.chess.Knight;
import com.xm.chess.Position;
import com.xm.chess.Solution;
import com.xm.dfs.IterativeDeepeningSearch;
import com.xm.dfs.Node;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Application {

    private static final int DEPTH_LIMIT_DEFAULT = 3;

    public static void main(String[] args) {
        try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            while(true) {
                System.out.print("Enter current knight position (A2 for example): ");
                String currentPos = br.readLine();

                System.out.print("Enter goal knight position (B5 for example): ");
                String goalPos = br.readLine();
                if (check(goalPos) && check(currentPos)) {
                    Position currentPosition = new Position(currentPos);
                    Position goalPosition = new Position(goalPos);

                    Node initNode = new Node(null, new Knight(currentPosition), 0);

                    IterativeDeepeningSearch dfs = new IterativeDeepeningSearch();
                    Solution solution = dfs.findSolution(initNode, DEPTH_LIMIT_DEFAULT, node -> node.getFigure().getCurrentPosition().equals(goalPosition));
                    if (solution != null) {
                        System.out.println("Next solutions was founded: ");
                        System.out.println(solution);
                    } else {
                        System.out.println("The task has no solution");
                    }
                    System.out.print("If you want to exit please input q: ");
                    String exit = br.readLine();
                    if(exit.equals("q")){
                        break;
                    }
                } else {
                    System.out.println("Incorrect input. Please input only letter and number exiting position and try again");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean check(String position){
        Pattern p = Pattern.compile("[A-H][1-8]");
        Matcher m = p.matcher(position);
        return m.matches();
    }

}
